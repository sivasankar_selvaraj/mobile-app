angular.module('app.services', [])

.factory('BlankFactory', [function(){

}])

.service('LoginService', function($q) {
    return {
        loginUser: function(mobile,res_mobile) {
            var deferred = $q.defer();
            var promise = deferred.promise;
 
            if (mobile == res_mobile) {
                deferred.resolve('Welcome To Home Food!');
            } else {
                deferred.reject('Wrong credentials.');
            }
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})

.service('BlankService', [function(){

}]);

