angular.module('app.controllers', [])

.controller('homeCtrl', function($scope, $localstorage, $state, $http) {
    $scope.area = 0;
    $scope.items = [];
    $scope.areas = [];
    $scope.totals = [];
    $scope.totalstotal = [];
    $http.get("http://sarvohandball.in/rest/v1/api.php?rquest=todayorders").success(function(response) {
        if (response.length > 0) {
            angular.forEach(response, function(value, key) {
                if ($scope.items.indexOf(value['type']) == -1) {
                    $scope.items.push(value['type']);
                }
                if ($scope.areas.indexOf(value['areaname']) == -1) {
                    $scope.areas.push(value['areaname']);
                }

                if (angular.isUndefined($scope.totals[value['areaname']])) {
                    $scope.totals[value['areaname']] = [];   
                }

                $scope.totals[value['areaname']].push({
                    type : value['type'],
                    total : value['total']
                });

            });

            for (var j = 0; j < $scope.items.length; j++) {
                $scope.totalstotal[$scope.items[j]] = 0;
            }

            for (var key in $scope.totals) {
                for (var i = 0; i < $scope.items.length; i++) {
                    $scope.totalstotal[$scope.totals[key][i].type] += parseInt($scope.totals[key][i].total);
                }
            }

            console.log($scope.totalstotal);

        } else {
            console.log('no');
        }
    });


    $http.get("http://sarvohandball.in/rest/v1/api.php?rquest=area").success(function(response) {
        if (response.length > 0) {
            $scope.area = response;
        } else {
            console.log('no');
        }
    });

    $scope.orders = function(id) {
        $localstorage.set('area_id', id);
        $state.go('myOrder');
    };
})

.controller('myOrderCtrl', function($scope, $state, $http, $localstorage, $filter) {
    if ($localstorage.get('area_id')) {
        $area_id = $localstorage.get('area_id');
    }
    $scope.extra = [];
    itemnames = [];
    customers = [];
    $scope.customerids = [];
    $http.get("http://sarvohandball.in/rest/v1/api.php?rquest=orders&areaid=" + $area_id).success(function(response) {
        if (response.length > 0) {
            angular.forEach(response, function(value, key) {
                $customer_keys = value['customerid'];
                if (angular.isUndefined($scope.extra[$customer_keys])) {
                    $scope.customerids.push(value['customerid']);
                    customers[$customer_keys] = [];
                    customers[$customer_keys]['Amount'] = 0;
                    $scope.extra[$customer_keys] = [];
                    $address = value['address'].replace(/undefined/g, '');
                    $scope.extra[$customer_keys].push({
                        Customer: value['customername'],
                        Mobile: value['mobile'],
                        Address: $address,
                        Delivery: value['deliverytime'],
                        Amount: 0
                    });
                }
                if (angular.isUndefined(customers[$customer_keys][value['itemname']])) {
                    customers[$customer_keys][value['itemname']] = 0;
                }

                if (itemnames.indexOf(value['itemname']) == -1) {
                    itemnames.push(value['itemname']);
                }

                customers[$customer_keys][value['itemname']] += parseInt(value['quantity']);
                customers[$customer_keys]['Amount'] += parseInt(value['itemprice']);

            });

            for (var key in $scope.extra) {
                $scope.extra[key][0]['Amount'] = customers[key]['Amount'];
                for (var i = 0; i < itemnames.length; i++) {
                    if (customers[key][itemnames[i]]) {
                        $scope.extra[key][0][itemnames[i]] = customers[key][itemnames[i]];
                    }
                }
            }

        } else {
            console.log('no');
        }
    });

})
