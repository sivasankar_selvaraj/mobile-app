angular.module('DataProviderService', [])
    .service("DataProvider", function($http, $q, $ionicLoading, CacheFactory) {

        //Caching configuration
        //60 * 60 * 24000: One day
        CacheFactory("itemsCache", {
            storageMode: "localStorage",
            maxAge: 60 * 60 * 1000,
            deleteOnExpire: "aggressive"
        });
        CacheFactory("loginCache", {
            storageMode: "localStorage",
            maxAge: 60 * 60 * 1000,
            deleteOnExpire: "aggressive"
        });
        //60 * 60 * 24000

        //Menu-Item. 
        self.itemsCache = CacheFactory.get("itemsCache");
        //User Data
        self.loginCache = CacheFactory.get("loginCache");

        // To deal with Internet failure
        self.itemsCache.setOptions({
            onExpire: function(key, value) {
                getCategories()
                    .then(function() {
                        console.log("Categories cache was automatically refreshed");
                    }, function() {
                        console.log("Error getting the categories data. Using the expired data");
                        self.itemsCache.put(key, value);
                    });
            }
        });

        // To deal with Internet failure
        self.loginCache.setOptions({
            onExpire: function(key, value) {
                getLoginCache(key)
                    .then(function() {
                        console.log("Sub categories cache was automatically refreshed");
                    }, function() {
                        console.log("Error getting the sub categories data. Using the expired data");
                        self.loginCache.put(key, value);
                    });
            }
        });

        function getItems(uri) {
            var deferred = $q.defer(),
                cacheKey = uri,
                itemsData = self.itemsCache.get(cacheKey);

            if (itemsData) {
                console.log("Categories data already available in the cache");
                deferred.resolve(itemsData);
            } else {

                $ionicLoading.show({
                    template: "Loading..."
                })

                $http.get(uri)
                    .success(function(response) {
                        console.log("Received Items  via HTTP");
                        self.itemsCache.put(cacheKey, response)
                        $ionicLoading.hide();
                        deferred.resolve(response);
                    })
                    .error(function() {
                        console.log("Error while making HTTP call for Items data")
                        $ionicLoading.hide();
                        deferred.resolve({
                            "showErrorMsg": "No internet connection! Make sure you are connected to the internet and retry"
                        });
                    });
            }
            return deferred.promise;
        }

        function getLoginCache(uri) {

            var deferred = $q.defer(),
                cacheKey = uri,
                loginCache = self.loginCache.get(cacheKey);

            if (loginCache) {
                console.log("Login data already available in the cache");
                deferred.resolve(loginCache);
            } else {

                $ionicLoading.show({
                    template: "Loading..."
                })

                $http.get(uri)
                    .success(function(response) {
                        console.log("Received Login data via HTTP");
                        self.loginCache.put(cacheKey, response)
                        $ionicLoading.hide();
                        deferred.resolve(response);
                    })
                    .error(function() {
                        console.log("Error while making HTTP call login data")
                        $ionicLoading.hide();
                        deferred.resolve({ "showErrorMsg": "No internet connection! Make sure you are connected to the internet and retry" });
                    });

            }
            return deferred.promise;
        };
        return {
            getItems: getItems,
            getLoginCache: getLoginCache
        };

    });
