angular.module('app.controllers', [])

.run(function($rootScope) {
    $rootScope.items = [];
    $rootScope.deliveryTime = "";
})


.controller('homeCtrl', function($scope, $rootScope, $localstorage, $state) {
    $scope.is_loged = function() {
        if ($localstorage.get('customer_mobile')) {
            $state.go('menu.todayMenu');
        } else {
            $state.go('login');
        }
    };
})

.controller('todayMenuCtrl', function($scope, $rootScope, $http, $ionicLoading, ionicTimePicker, DataProvider, ionicToast) {
    $scope.loading = true;
    $scope.noorder = false;
    $scope.no_data_footer = true;
    $scope.tabs = [
        { "text": "Dinner" },
        { "text": "Lunch" },
        { "text": "Special" }
    ];
    $scope.onSlideMove = function(data) {
        $scope.items = 0;
        $scope.loading = true;
        $scope.noorder = false;
        $scope.no_data_footer = true;
        if (data.index == 0) {
            $url = "http://sarvohandball.in/rest/v1/api.php?rquest=menu&closedyn=0&category=DINNER";
        } else if (data.index == 1) {
            $url = "http://sarvohandball.in/rest/v1/api.php?rquest=menu&closedyn=0&category=LUNCH";
        } else {
            $url = "http://sarvohandball.in/rest/v1/api.php?rquest=menu&closedyn=0&category=SPECIAL";
        }

        DataProvider.getItems($url).then(function(data) {
            if (data.hasOwnProperty('showErrorMsg')) {
                $scope.loading = false;
                $scope.noorder = true;
                $scope.no_data_footer = true;
                $scope.nextbutton = 'none';
                ionicToast.show(data.showErrorMsg, 'bottom', true, 2500);
            } else {
                $scope.nextbutton = 'none';
                $scope.items = data;

                // Initializing the quantity of all the items.
                angular.forEach($scope.items, function(value, key) {
                    angular.forEach(value, function() {
                        value['value'] = '0';
                        value['total_price'] = '0';
                    });
                });

                if ($scope.items.length > 0) {
                    // If there are no open items available (closedyn = Y),
                    $scope.loading = false;
                    $scope.noorder = false;
                    $scope.no_data_footer = false;
                    $scope.nextbutton = 'block';
                } else {
                    $scope.loading = false;
                    $scope.noorder = true;
                    $scope.no_data_footer = true;
                }

                $rootScope.items = $scope.items;
            }
        });

        $scope.decrement = function(item) {
            if (item.value > 0) {
                item.value--;
                item.total_price = item.price * item.value;
            }
        };

        $scope.increment = function(item) {
            item.value++;
            item.total_price = item.price * item.value;
        };

        $scope.openTime = function() {
            // Set the time if have previous time, else set default time
            if ($rootScope.deliveryTime) {
                $previous_time = $rootScope.deliveryTime.split(':');
                $previous_time = (($previous_time[0] * 60) + parseInt($previous_time[1])) * 60;
            } else {
                $previous_time = 50400;
            }

            var ipObj1 = {
                callback: function(val) {
                    if (typeof(val) === 'undefined') {
                        console.log('Time not selected');
                    } else {
                        var selectedTime = new Date(val * 1000);
                        var UTCminutes = selectedTime.getUTCMinutes();
                        var UTChours = selectedTime.getUTCHours();
                        var hours = UTChours;
                        var minutes = UTCminutes;

                        // Adding zeros if minutes or hours less than 10
                        if (UTChours < 10) {
                            hours = "0" + UTChours;
                        }

                        if (UTCminutes < 10) {
                            minutes = "0" + UTCminutes;
                        }

                        $rootScope.deliveryTime = hours + ":" + minutes + ":00";
                    }
                },
                step: 1,
                inputTime: $previous_time,
            };

            ionicTimePicker.openTimePicker(ipObj1);

        }
    };
})

.controller('orderConfirmationCtrl', function($scope, $rootScope, $state, $http, $localstorage, ionicToast) {

    $scope.items = $rootScope.items;
    $scope.total_price = 0;
    if ($rootScope.deliveryTime) {
        $scope.deliveryTime = "Delivery Time : " + $rootScope.deliveryTime;
    } else {
        $scope.deliveryTime = "Will be delivered at the earliest...";
    }

    // This must be refactored
    $scope.orders = 'none';
    $scope.noorders = 'block';

    angular.forEach($scope.items, function(value, key) {
        angular.forEach(value, function(value, key) {
            if (key == 'total_price' && value != '0') {
                $scope.total_price = value + $scope.total_price;
            }

            if (key == 'value' && value != '0') {
                $scope.orders = 'block';
                $scope.noorders = 'none';

            }
        });
    });

    $scope.submitorder = function() {

        var customerid = $localstorage.get('customer_id');
        var dataArray = [];

        angular.forEach($scope.items, function(value, key) {
            var data = "rquest=postorder&";
            var isquantity = '1';
            angular.forEach(value, function(value, key) {
                if (key == 'id') {
                    data += "itemid=" + value + "&";
                }
                if (key == 'value') {
                    data += "quantity=" + value + "&";
                    if (value == '0') {
                        isquantity = '0';
                    }
                }
                if (key == "deliverydate") {
                    data += key + "=" + value + "&";
                }
            });
            data += "customerid=" + customerid + "&deliverytime=" + $rootScope.deliveryTime;

            if (isquantity != '0') {
                dataArray.push(data);
            }
        });

        statusmessage = '';
        for (var i = 0; i < dataArray.length; i++) {
            var req = {
                method: 'POST',
                url: "http://sarvohandball.in/rest/v1/api.php",
                data: dataArray[i],
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            };

            $http(req).success(function(data, status, headers, config) {
                statusmessage = data.status;
                if (data.status == 'Success') {
                    if (i + 1 == dataArray.length) {
                        $state.go('menu.myOrder');
                    }
                    $state.go('menu.myOrder');
                    console.log(data);
                }
            }).error(function(data, status, headers, config) {
                statusmessage = 'Failure';
                ionicToast.show("No internet connection! Make sure you are connected to the internet and retry", 'bottom', true, 2500);
            });
        }

    }


})

.controller('loginCtrl', function($scope, $ionicPopup, $state, $localstorage, $http, DataProvider, ionicToast) {
    $scope.data = {};
    $scope.res = "";
    $scope.login = function() {
        console.log($scope.data.mobile);
        if (typeof($scope.data.mobile) === 'undefined') {
            var alertPopup = $ionicPopup.alert({
                title: 'Mobile number cannot be empty!',
                template: 'Please enter your valid mobile number.'
            });
        } else if (/^[0-9]{10,10}$/.test($scope.data.mobile)) {
            $url = "http://sarvohandball.in/rest/v1/api.php?rquest=customers&mobile=" + $scope.data.mobile;
            DataProvider.getLoginCache($url).then(function(data) {
                console.log(data);
                if (data.hasOwnProperty('showErrorMsg')) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error Occurred!',
                        template: 'Please try again later.'
                    });
                    ionicToast.show(data.showErrorMsg, 'bottom', true, 2500);
                } else {
                    $scope.res = data;
                    if ($scope.res.length > 0) {
                        angular.forEach($scope.res, function(value, key) {
                            $scope.res_mobile_no = value['mobile'];
                            $localstorage.set('customer_name', value['name']);
                            $localstorage.set('customer_email', value['email']);
                            $localstorage.set('customer_doornumber', value['doornumber']);
                            $localstorage.set('customer_mobile', $scope.res_mobile_no);
                            $localstorage.set('customer_address1', value['address1']);
                            $localstorage.set('customer_address2', value['address2']);
                            $localstorage.set('customer_areaid', value['areaid']);
                            $localstorage.set('customer_id', value['id']);
                        });
                        $state.go('menu.todayMenu');

                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Login failed!',
                            template: 'Please check your mobile number.'
                        });
                    }
                }
            });
        } else {

            var alertPopup = $ionicPopup.alert({
                title: 'Invalid mobile number!',
                template: 'Please check your mobile number.'
            });
        }

    }

})

.controller('profileCtrl', function($scope, $state, $ionicPopup, $rootScope, $http, $localstorage, ionicToast) {

    $scope.hidebar = "true";
    $scope.authorization = {};

    $scope.authorization.username = $localstorage.get('customer_name').replace(/undefined/g, '');
    $scope.authorization.email = $localstorage.get('customer_email').replace(/undefined/g, '');
    $scope.authorization.mobile = $localstorage.get('customer_mobile').replace(/undefined/g, '');
    $scope.authorization.door_no = $localstorage.get('customer_doornumber').replace(/undefined/g, '');
    $scope.authorization.appartment = $localstorage.get('customer_address1').replace(/undefined/g, '');
    $scope.authorization.landmark = $localstorage.get('customer_address2').replace(/undefined/g, '');
    $scope.authorization.areaid = $localstorage.get('customer_areaid').replace(/undefined/g, '');

    if ($scope.authorization.appartment == '-') {

        $scope.authorization.appartment = $scope.authorization.appartment.replace(/-/g, '');

    }

    if ($localstorage.get('customer_mobile')) {
        $scope.hidebar = "false";
    }

    $http.get("http://sarvohandball.in/rest/v1/api.php?rquest=area").success(function(response) {
        $scope.areaNames = response;
    });

    if ($scope.authorization.mobile != '') {
        $scope.buttonname = "Update";
        $scope.authorization.actionfunction = "profileUpdate";
    } else {
        $scope.buttonname = "Create";
        $scope.authorization.actionfunction = "signIn";
    }

    $scope.profile = function(form) {
        if (form.actionfunction.$viewValue == "signIn") {
            $scope.signIn(form);
        } else {
            $localstorage.set('customer_mobile', form.mobile.$viewValue);
            $localstorage.set('customer_name', form.username.$viewValue);
            $localstorage.set('customer_email', form.email.$viewValue);
            $localstorage.set('customer_doornumber', form.door_no.$viewValue);
            $localstorage.set('customer_address1', form.appartment.$viewValue);
            $localstorage.set('customer_address2', form.landmark.$viewValue);
            $localstorage.set('customer_areaid', form.areaid.$viewValue);
            $localstorage.set('customer_id', $localstorage.get('customer_id'));
            $scope.profileUpdate(form);
        }
    };


    $scope.signIn = function(form) {
        if (form.$valid) {
            myobject = {
                rquest: 'signup',
                name: form.username.$viewValue,
                email: form.email.$viewValue,
                mobile: form.mobile.$viewValue,
                doornumber: form.door_no.$viewValue,
                address1: form.appartment.$viewValue,
                address2: form.landmark.$viewValue,
                areaid: form.areaid.$viewValue
            };
            Object.toparams = function ObjecttoParams(obj) {
                var p = [];
                for (var key in obj) {
                    p.push(key + '=' + encodeURIComponent(obj[key]));
                }
                return p.join('&');
            };

            var req = {
                method: 'POST',
                url: "http://sarvohandball.in/rest/v1/api.php",
                data: Object.toparams(myobject),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            };
            statusmessage = '';
            $http(req).success(function(data, status, headers, config) {
                statusmessage = data.status;
                $localstorage.set('customer_id', data.customerid);
                if (data.status == 'Success') {
                    $localstorage.set('customer_mobile', form.mobile.$viewValue);
                    $localstorage.set('customer_name', form.username.$viewValue);
                    $localstorage.set('customer_email', form.email.$viewValue);
                    $localstorage.set('customer_doornumber', form.door_no.$viewValue);
                    $localstorage.set('customer_address1', form.appartment.$viewValue);
                    $localstorage.set('customer_address2', form.landmark.$viewValue);
                    $localstorage.set('customer_areaid', form.areaid.$viewValue);
                    $state.go('menu.todayMenu');
                } else if (data.status == 'Duplicate') {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Account already available!',
                        template: 'Please try again with different mobile number.'
                    });
                }
            }).error(function(data, status, headers, config) {
                statusmessage = 'Failure';
                ionicToast.show("No internet connection! Make sure you are connected to the internet and retry", 'bottom', true, 2500);
            });
        }
    };

    $scope.profileUpdate = function(form) {
        if (form.$valid) {
            myobject = {
                rquest: 'profileupdate',
                name: form.username.$viewValue,
                email: form.email.$viewValue,
                mobile: form.mobile.$viewValue,
                doornumber: form.door_no.$viewValue,
                address1: form.appartment.$viewValue,
                address2: form.landmark.$viewValue,
                areaid: form.areaid.$viewValue,
                id: $localstorage.get('customer_id'),
            };
            Object.toparams = function ObjecttoParams(obj) {
                var p = [];
                for (var key in obj) {
                    p.push(key + '=' + encodeURIComponent(obj[key]));
                }
                return p.join('&');
            };

            var req = {
                method: 'POST',
                url: "http://sarvohandball.in/rest/v1/api.php",
                data: Object.toparams(myobject),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            };

            statusmessage = '';
            $http(req).success(function(data, status, headers, config) {
                statusmessage = 'Success';
                if (data.status == 'Success') {
                    $state.go('menu.todayMenu');
                }
            }).error(function(data, status, headers, config) {
                statusmessage = 'Failure';
                ionicToast.show("No internet connection! Make sure you are connected to the internet and retry", 'bottom', true, 2500);
            });
        }
    };
})

.controller('thankYouCtrl', function($scope) {

})

.controller('myOrderCtrl', function($scope, $state, $rootScope, $http, $localstorage) {
    $scope.order = [];
    $scope.deliverydates = [];
    $scope.totalamounts = [];

    var customerid = $localstorage.get('customer_id');

    $http.get("http://sarvohandball.in/rest/v1/api.php?rquest=myorders&customerid=" + customerid).success(function(response) {
        if (response.length > 0) {

            angular.forEach(response, function(value, key) {
                $order_key = value['deliverydate'];
                if (angular.isUndefined($scope.order[$order_key])) {
                    $scope.order[$order_key] = [];
                    $scope.totalamounts[$order_key] = 0;
                }
                $scope.order[$order_key].push({
                    name: value['name'],
                    quantity: value['totalquantity']
                });

                $scope.totalamounts[$order_key] = parseInt($scope.totalamounts[$order_key]) + parseInt(value['totalamount']);

            });

            for (var keyName in $scope.order) {
                $scope.deliverydates.push({
                    date: keyName,
                    rate: $scope.totalamounts[keyName]
                });
            }
        }
    });
})

.controller('menuCtrl', function($scope) {
    $scope.logout = function() {
        console.log('sss');
    };
})

.controller('registerCtrl', function($scope) {

})
