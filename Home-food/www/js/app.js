// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

angular.module('app', ['ionic', 'angular-cache', 'tabSlideBox', 'app.controllers', 'app.routes', 'DataProviderService', 'app.directives', 'ionic-timepicker', 'ngMessages', 'ionic-toast'])

.factory('$localstorage', ['$window', function($window) {
    return {
        // primitive value
        set: function(key, value) {
            $window.localStorage[key] = value;
        },
        // primitive value
        get: function(key, defaultValue) {
            return $window.localStorage[key] || "";
        },
        // value will be JSON object
        setObject: function(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        // value will be JSON object
        getObject: function(key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    }
}])

.run(function($ionicPlatform, CacheFactory, $localstorage, $ionicPopup) {


    $ionicPlatform.ready(function() {


        var deploy = new Ionic.Deploy();
        deploy.watch().then(function() {}, function() {}, function(updateAvailable) {
            if (updateAvailable) {
                deploy.download().then(function() {
                    deploy.extract().then(function() {
                        deploy.unwatch();
                        $ionicPopup.show({
                            title: 'Update available',
                            subTitle: 'An update was just downloaded. Would you like to restart your app to use the latest features?',
                            buttons: [
                                { text: 'Not now' }, {
                                    text: 'Restart',
                                    onTap: function(e) {
                                        deploy.load();
                                    }
                                }
                            ]
                        });
                    });
                });
            }
        });
        // Onesignal for push notifications
        // start the push notification event listener
        var notificationOpenedCallback = function(jsonData) {
            console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
        };
        window.plugins.OneSignal.init("aa3abb64-46d0-49c9-ba45-078efb4bf861", { googleProjectNumber: "644361066599" },
            notificationOpenedCallback);
        window.plugins.OneSignal.enableInAppAlertNotification(true);


        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }



    });

})

.config(function(ionicTimePickerProvider) {
    var timePickerObj = {
        inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
        format: 12,
        step: 15,
        setLabel: 'Set',
        closeLabel: 'Close'
    };
    ionicTimePickerProvider.configTimePicker(timePickerObj);
})
