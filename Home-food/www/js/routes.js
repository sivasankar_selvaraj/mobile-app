angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

  .state('home', {
    url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'    
  })

  .state('menu.todayMenu', {
    url: '/today_menu',
    views: {
      'side-menu21': {
        templateUrl: 'templates/todayMenu.html',
        controller: 'todayMenuCtrl'
      }
    }
  })

  .state('menu.orderConfirmation', {
    url: '/order-conform',
    views: {
      'side-menu21': {
        templateUrl: 'templates/orderConfirmation.html',
        controller: 'orderConfirmationCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu',
    templateUrl: 'templates/menu.html',
    abstract:true
  })

  .state('login', {
    url: '/login',    
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('register', {
    url: '/register',   
        templateUrl: 'templates/register.html',
        controller: 'registerCtrl'     
  })

  .state('menu.profile', {
    url: '/profile',
    views: {
      'side-menu21': {
        templateUrl: 'templates/profile.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('menu.myOrder', {
    url: '/my-order',
    views: {
      'side-menu21': {
        templateUrl: 'templates/myOrder.html',
        controller: 'myOrderCtrl'
      }
    }
  })

  .state('menu.thankYou', {
    url: '/thank-you',
    views: {
      'side-menu21': {
        templateUrl: 'templates/thankYou.html',
        controller: 'thankYouCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/home')

  

});